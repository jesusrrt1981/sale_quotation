# -*- coding: utf-8 -*-
{
    "name": "Sale Quotation",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",

    'depends': ['base','sale_management',],


    'data': [
 
        'views/views.xml',
        'views/templates.xml',
    ],
  
    'demo': [
        'demo/demo.xml',
    ],
}
